# 2018 Marganai forest soil erosion experiment database

A Quarto file extensivly presenting the database, produced using NFS-DDP, to assemle and organize the data collected in the frame of a forest soil erosion evaluation experiment.

The database is presented as an example of reproducible research, utilizing Quarto (a markdown-based tool that integrates formatted text, code, and code outputs). The database is structured using a relationally verified SQLite file, which addresses relevant FAIRness problems often encountered re-using spreadsheets or CSV file collections.

The '2018 Marganai Forest Soil Erosion Experiment Database' is a comprehensive repository of data collected during scientific experiment trials designed to investigate the effects of forest canopy coverage on soil erosion under intense artificial rainfall. The investigation, conducted in the Marganai forest in Sardinia, Italy, involved the establishment of eight paired plots with and without forest canopy coverage, subjected to artificial rainfall simulation. The database contains detailed information on the experimental setup, soil characteristics and rainfall simulation, as well as the resulting data on runoff water and soil erosion.

The trials aimed to experimentally measure the amount of sediment transported by rainfall runoff water, with a focus on the impact of forest canopy coverage on soil erosion. The data show that the presence of forest canopy coverage significantly reduces soil erosion, with maximum runoff water rates of 2 mm/hour observed in plots without canopy coverage. This database provides a valuable resource for researchers and practitioners interested in understanding the complex relationships between forest management practices, soil erosion, and water runoff. Future research directions may include the of machine learning algorithms to analyze the data and predict soil erosion patterns, as well as the integration of additional data sources to further refine our understanding of these complex interactions.

The paper represents an example of reproducible research, it is produced by the Quarto file hoseted in the repository https://gitlab.com/NuoroForestrySchool/nfs-data_documentation_procedure-examples/2018-marganai-forest-soil-erosion-experiment-database.git, and it explois the NFS-DPP output, a validated database including detailed metadata.

Keywords
- Soil erosion
- Forest canopy coverage
- Artificial rainfall simulation
- Database
- Reproducible research
- FAIR data

Short summary

The 2018 Marganai Forest Soil Erosion Experiment Database presents the results of a scientific investigating the effects of forest canopy coverage on soil erosion under intense artificial rainfall. The study, conducted in the Marganai forest in Sardinia, Italy, involved paired plots with and without forest canopy coverage. The results show that forest canopy coverage significantly reduces soil erosion, with maximum runoff water rates of 2 mm/hour observed in plots without canopy coverage. The work is presented as an example of reproducible research, utilizing Quarto and an SQLite file to increase FAIRness.